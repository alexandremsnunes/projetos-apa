#include <stdio.h>
#include <stdlib.h>


//Wikipedia
void merge(int vetor[], int comeco, int meio, int fim) {

    int com1 = comeco, com2 = meio+1, comAux = 0, tam = fim-comeco+1;

    int *vetAux;

    vetAux = (int*)malloc(tam * sizeof(int));


    while(com1<=meio && com2<=fim){

        if(vetor[com1] <= vetor[com2]){

            vetAux[comAux] = vetor[com1];

            com1++;

        }else{

            vetAux[comAux] = vetor[com2];

            com2++;

        }

        comAux++;

    }


    while(com1<=meio){  //Caso ainda haja elementos na primeira metade

        vetAux[comAux] = vetor[com1];

        comAux++;com1++;

    }


    while(com2<=fim){   //Caso ainda haja elementos na segunda metade

        vetAux[comAux] = vetor[com2];

        comAux++;com2++;

    }


    for(comAux=comeco;comAux<=fim;comAux++){    //Move os elementos de volta para o vetor original

        vetor[comAux] = vetAux[comAux-comeco];

    }

    

    free(vetAux);

}


void mergeSort(int vetor[], int comeco, int fim){

    if (comeco < fim) {

        int meio = (fim+comeco)/2;


        mergeSort(vetor, comeco, meio);

        mergeSort(vetor, meio+1, fim);

        merge(vetor, comeco, meio, fim);

    }

}

int contaLinhas(){
	FILE *arquivo = fopen("couting.txt", "r");
	int caractere, existe_linhas = 0;
  	int quant_linhas = 0;
  
  	while((caractere = fgetc(arquivo)) != EOF){
    	existe_linhas = 3; 
    
    	if(caractere == '\n'){ 
      		quant_linhas++;             
    	} 
  	}

  if(existe_linhas){
    quant_linhas ++;
  }  

 //printf("O arquivo possui %d linhas.\n",quant_linhas );

 fclose(arquivo);
 return quant_linhas;
}




void criaVetordeEntrada(int tam){
	FILE *arquivo = fopen("couting.txt", "r");
	FILE *arquivo2 = fopen("saida.txt", "w");

	int i = 0;
	int aux;
	int vetorentrada[tam];

	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < tam; i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	//insertionSort(vetorentrada, tam);
	mergeSort(vetorentrada, 0, (tam-1));
	//for(i = 0; i < tam; i++){
	//	printf("\n %d",vetorentrada[i]);
	//}
	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}
	

	printf("\n vetor criado com sucesso");
	fclose(arquivo);
	fclose(arquivo2);
}

int main(){
	int tam;

	tam = contaLinhas();

	//int entrada[tam];
	criaVetordeEntrada(tam);
	printf("\nTerminou...");

	return 0;
}