#include <stdio.h>
#include <stdlib.h>



//Wikipedia
void radixsort(int vetor[], int tamanho) {
    int i;
    int *b;
    int maior = vetor[0];
    int exp = 1;

    b = (int *)calloc(tamanho, sizeof(int));

    for (i = 0; i < tamanho; i++) {
        if (vetor[i] > maior)
    	    maior = vetor[i];
    }

    while (maior/exp > 0) {
        int bucket[10] = { 0 };
    	for (i = 0; i < tamanho; i++)
    	    bucket[(vetor[i] / exp) % 10]++;
    	for (i = 1; i < 10; i++)
    	    bucket[i] += bucket[i - 1];
    	for (i = tamanho - 1; i >= 0; i--)
    	    b[--bucket[(vetor[i] / exp) % 10]] = vetor[i];
    	for (i = 0; i < tamanho; i++)
    	    vetor[i] = b[i];
    	exp *= 10;
    }

    free(b);
}

int contaLinhas(){
	FILE *arquivo = fopen("couting.txt", "r");
	int caractere, existe_linhas = 0;
  	int quant_linhas = 0;
  
  	while((caractere = fgetc(arquivo)) != EOF){
    	existe_linhas = 3; 
    
    	if(caractere == '\n'){ 
      		quant_linhas++;             
    	} 
  	}

  if(existe_linhas){
    quant_linhas ++;
  }  

 //printf("O arquivo possui %d linhas.\n",quant_linhas );

 fclose(arquivo);
 return quant_linhas;
}




void criaVetordeEntrada(int tam){
	FILE *arquivo = fopen("couting.txt", "r");
	FILE *arquivo2 = fopen("saidaRadixSort.txt", "w");

	int i = 0;
	int aux;
	int vetorentrada[tam];

	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < tam; i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	radixsort(vetorentrada, tam);

	
	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}
	

	printf("\n vetor criado com sucesso");
	fclose(arquivo);
	fclose(arquivo2);
}

int main(){
	int tam;

	tam = contaLinhas();

	//int entrada[tam];
	criaVetordeEntrada(tam);
	

	return 0;
}