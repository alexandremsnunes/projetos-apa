#include <stdio.h>
#include <stdlib.h>


//Wikipedia
void swap(int v[], int j, int aposJ) {
        int aux = v[j];
        v[j] = v[aposJ];
        v[aposJ] = aux;
}

void maxHeapify(int vetor[], int pos, int tamanhoDoVetor) {
 
    int max = 2 * pos + 1, right = max + 1;
    if (max < tamanhoDoVetor) {
 
        if (right < tamanhoDoVetor && vetor[max] < vetor[right]) {
            max = right;
        }
 
        if (vetor[max] > vetor[pos]) {
            swap(vetor, max, pos);
            maxHeapify(vetor, max, tamanhoDoVetor);
        }
    }
}
 
void buildMaxHeap(int v[], int tam) {
    for (int i = tam / 2 - 1; i >= 0; i--) {
        maxHeapify(v, i, tam);
    }
 
}

void heapSort(int v[],int tam){
    buildMaxHeap(v,tam);
    int n = tam;
 
    for (int i = tam - 1; i > 0; i--) {
        swap(v, i, 0);
        maxHeapify(v, 0, --n);
    }
  
}
 
 


int contaLinhas(){
	FILE *arquivo = fopen("couting.txt", "r");
	int caractere, existe_linhas = 0;
  	int quant_linhas = 0;
  
  	while((caractere = fgetc(arquivo)) != EOF){
    	existe_linhas = 3; 
    
    	if(caractere == '\n'){ 
      		quant_linhas++;             
    	} 
  	}

  if(existe_linhas){
    quant_linhas ++;
  }  

 
 fclose(arquivo);
 return quant_linhas;
}




void criaVetordeEntrada(int tam){
	FILE *arquivo = fopen("couting.txt", "r");
	FILE *arquivo2 = fopen("saidaheap.txt", "w");

	int i = 0;
	int aux;
	int vetorentrada[tam];
	
	
	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < tam; i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	heapSort(vetorentrada, tam);
    
	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}
	

	printf("\n vetor criado com sucesso");
	fclose(arquivo);
	fclose(arquivo2);
}

int main(){
	int tam;

	tam = contaLinhas();
	//tam = (sizeof(v[])/sizeof[0]);
	criaVetordeEntrada(tam);
	printf("\nTerminou...");

	return 0;
}