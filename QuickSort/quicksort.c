#include <stdio.h>
#include <stdlib.h>



//Wikipedia

int partition( int a[], int l, int r) {
   int pivot, i, j, t;
   pivot = a[l];
   i = l; j = r+1;
		
   while( 1)
   {
   	do ++i; while( a[i] <= pivot && i <= r );
   	do --j; while( a[j] > pivot );
   	if( i >= j ) break;
   	t = a[i]; a[i] = a[j]; a[j] = t;
   }
   t = a[l]; a[l] = a[j]; a[j] = t;
   return j;
}


void quickSort( int a[], int l, int r){
   int j;

   if( l < r ) 
   {
   	// divide and conquer
        j = partition( a, l, r);
       quickSort( a, l, j-1);
       quickSort( a, j+1, r);
   }
	
}


int contaLinhas(){
	FILE *arquivo = fopen("couting.txt", "r");
	int caractere, existe_linhas = 0;
  	int quant_linhas = 0;
  
  	while((caractere = fgetc(arquivo)) != EOF){
    	existe_linhas = 3; 
    
    	if(caractere == '\n'){ 
      		quant_linhas++;             
    	} 
  	}

  if(existe_linhas){
    quant_linhas ++;
  }  

 //printf("O arquivo possui %d linhas.\n",quant_linhas );

fclose(arquivo);
 return quant_linhas;
}



void criaVetordeEntrada(int tam){
	FILE *arquivo = fopen("couting.txt", "r");
	FILE *arquivo2 = fopen("saida.txt", "w");

	int i = 0;
	int aux;
	int vetorentrada[tam];

	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < tam; i++){ 
			fscanf(arquivo,"%d", &aux);
				vetorentrada[i] = aux; 
		}
	}

	quickSort( vetorentrada, 0, (tam-1));
	//for(i = 0; i < tam; i++){
	//	printf("\n %d",vetorentrada[i]);
	//}
	for(i = 0; i < tam; i++){
		fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	}
	

	printf("\n vetor criado com sucesso");
	fclose(arquivo);
	fclose(arquivo2);
}

int main(){
	int tam;

	tam = contaLinhas();

	//int entrada[tam];
	criaVetordeEntrada(tam);
	printf("\nQuantidade : %d\n", tam);
	return 0;
}