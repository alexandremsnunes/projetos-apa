#include <stdio.h>
#include <stdlib.h>

char arquivoentrada[] = "mochila01.txt";

void algoritmoMochila(int n, int M, int pesos[], int valores[]){
   int i, j;
   int matriz[n+1][M+1];

   for(i = 0; i <= n; i++){
   		for(j = 0; j <= M; j++){
           
           if(i==0 || j==0){
		   		matriz[i][j] = 0;
           }
           else if(pesos[i-1] <= j){
				matriz[i][j]=
				(valores[i-1] + matriz[i-1][j-pesos[i-1]] > matriz[i-1][j])?
				valores[i-1] + matriz[i-1][j-pesos[i-1]] : matriz[i-1][j];
           }     
           else{
           		matriz[i][j] = matriz[i-1][j];
           } 
	    }
   }
   
   printf("Valor: %d\n",matriz[n][M]);
    
}

void preparaInstancias(int vetor[], int tam){
	int i,j=0,k=0,n,M;
	n = vetor[0];
	M = vetor[1];
	int vetorP[n],vetorV[n];

	for (i = 2; i < tam; i++){
		if((i % 2) == 0){
			vetorP[k] = vetor[i];
			k++; 
		}
		else{
			vetorV[j] = vetor[i];
			j++;
		}		
	}
	algoritmoMochila(n,M,vetorP,vetorV);
}


int QtdeLinhas(){
    FILE *arquivo = fopen(arquivoentrada, "r");
    int existe_linhas = 0, numCaracteres=0, numPalavras=0, numLinhas=0, comecouPalavra=0;
    int caractere;
   
    while ((caractere = fgetc(arquivo)) != EOF) {
        numCaracteres++;
        if  ((caractere!=' ') && (caractere!='\n') && (!comecouPalavra)) {
            comecouPalavra = 1;
        }
        if  (((caractere==' ') || (caractere == '\n')) && (comecouPalavra)) {
            comecouPalavra = 0;
            numPalavras++;
        }
    }           
    
	fclose(arquivo);
  	return numPalavras;
}

void criaVetordeEntrada(int tam){
	FILE *arquivo = fopen(arquivoentrada, "r");
	//FILE *arquivo2 = fopen(arquivosaida, "w");
	int i = 0;
	int aux;
	int vetorentrada[tam];

	if(arquivo == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
	else{
		for (i = 0; i < tam; i++){ 
			fscanf(arquivo,"%d", &aux);
			vetorentrada[i] = aux; 
		}
	}

	preparaInstancias(vetorentrada, tam);

	/*
	for(i = 0; i < tam; i++){
		printf("\n %d",vetorentrada[i]);
	}*/
	//for(i = 0; i < tam; i++){
	//	fprintf(arquivo2, "%d\n",vetorentrada[i] );	
	//}
	

	printf("\nvetor criado com sucesso");
	fclose(arquivo);
	//fclose(arquivo2);
}

int main(){
	int tam;

	tam  =  QtdeLinhas();
	criaVetordeEntrada(tam);
	printf("\nQuantidade : %d\n", tam);

	return 0;
}